package main

//给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。
//你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。
//设计一个算法来计算你所能获取的最大利润。
//返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0

// 使用双指针的方式遍历所有可能
func maxProfit(prices []int) (ans int) {
	minPrice := prices[0]      //买入价格
	for _, p := range prices { //p卖出价格
		ans = maxans(minPrice, p, ans)
		minPrice = minans(minPrice, p)
	}
	return ans
}
func minans(minPrice int, p int) int {
	//找到最小买入点
	if minPrice > p {
		return p
	}
	return minPrice
}
func maxans(minPrice int, p int, ans int) int { //当收益小于0时候返回0
	if (p - minPrice) > ans { //有收益 切大于上次判断
		return (p - minPrice)
	}
	return ans
}

func main() {

}
