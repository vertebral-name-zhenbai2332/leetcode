package main

import (
	"fmt"
	"math"
)

// 给定一个二进制数组 nums ， 计算其中最大连续 1 的个数
func findMaxConsecutiveOnes(nums []int) (maxcnt int) {
	var cnt float64
	for _, i := range nums {
		if i == 1 {
			cnt++
		} else {
			maxcnt = int(math.Max(float64(maxcnt), cnt))
			cnt = 0
		}
	}
	maxcnt = int(math.Max(float64(maxcnt), cnt))
	return
}

func main() {
	aa := []int{1, 1, 1, 0, 1}
	aa = []int{1, 1, 1, 1, 0, 1}
	fmt.Println(findMaxConsecutiveOnes(aa))
}
